import tkinter as Tk
import serial.tools.list_ports as port_list
import serial
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure
import matplotlib.animation as animation
import matplotlib
from crccheck.crc import CrcModbus
from numpy import arange
import time
from threading import Thread
from numpy import arange, sin, pi


# http://patorjk.com/software/taag/#p=display&f=Roman

DEBUG = False

# ooooooooo.     .oooooo.   ooooooooo.   ooooooooooooo
# `888   `Y88.  d8P'  `Y8b  `888   `Y88. 8'   888   `8
#  888   .d88' 888      888  888   .d88'      888
#  888ooo88P'  888      888  888ooo88P'       888
#  888         888      888  888`88b.         888
#  888         `88b    d88'  888  `88b.       888
# o888o         `Y8bood8P'  o888o  o888o     o888o

def port_chosen(value):
    """Function called when port is chosen"""
    try:
        COM.close()
    except:
        pass
    COM.port = str(value).split()[0]
    COM.baudrate = 115200
    COM.timeout = None
    COM.open()
    time.sleep(2)
    if not COM.is_open:
        TEXT_LABEL['text'] = "Device unreachable"
        TEXT_LABEL['foreground'] = "red"
        raise Exception("Device unreachable")

    #global DELAY
    #DELAY = calibrate(COM)
    #global XS
    #XS = [x * DELAY for x in range(0, 5000)]

    PORT_MENU.pack_forget()

    TEXT_LABEL['text'] = "connected"
    TEXT_LABEL['foreground'] = "green"

    PERIOD_MENU = period_menu()
    TRESHOLD_MENU = treshold_menu()

    global CANVAS
    CANVAS = create_canvas()
    subplot = CANVAS.figure.get_axes()[0]
    subplot.grid(color=(0.5, 0.5, 0.5, 0.3), linestyle='-', linewidth=1)
    subplot.set_ylim(0,5)
    subplot.set_title(COM.name)

    CANVAS.show()
    toolbar = NavigationToolbar2TkAgg(CANVAS, APP_WINDOW)
    toolbar.update()



    global ANI
    ANI = animation.FuncAnimation(CANVAS.figure, animate, interval=33)

    period_chosen(35)
    treshold_chosen(2.5)
    global THREAD1
    THREAD1 = Thread(target=dataProcess)
    THREAD1.start()

PERIOD_MENU = None
THREAD1 = {}
COM = serial.Serial()
ANI = []


#       .o.       ooooo      ooo ooooo ooo        ooooo
#      .888.      `888b.     `8' `888' `88.       .888'
#     .8"888.      8 `88b.    8   888   888b     d'888
#    .8' `888.     8   `88b.  8   888   8 Y88. .P  888
#   .88ooo8888.    8     `88b.8   888   8  `888'   888
#  .8'     `888.   8       `888   888   8    Y     888
# o88o     o8888o o8o        `8  o888o o8o        o888o

DATA_AVAILABLE = False
TICK = time.time()

def animate(i):
    """Function rendering data"""

    if not PLAY:
        return
    global DATA_AVAILABLE

    if DATA_AVAILABLE:
        XS[:] = [x*(PERIOD/1000) for x in arange(-(BUFFERSIZE/2)+1, (BUFFERSIZE/2), 1)]
        subplot = CANVAS.figure.get_axes()[0]
        subplot.clear()
        subplot.grid(color=(0.5, 0.5, 0.5, 0.3), linestyle='-', linewidth=1)
        subplot.set_ylim(0,5)
        subplot.set_title(COM.name)
        subplot.set_xlabel("t [ms]")
        subplot.set_ylabel("U [V]")

        try:
            subplot.plot(XS[3:799], YS[3:799])
            subplot.plot(XS[3:799], ([TRESHOLD]*800)[3:799], 'y:')

        except:
            print("passed")
            pass
        DATA_AVAILABLE = False
        




PLAY = True



XS = []
YS = []


# oooooooooo.         .o.       ooooooooooooo       .o.
# `888'   `Y8b       .888.      8'   888   `8      .888.
#  888      888     .8"888.          888          .8"888.
#  888      888    .8' `888.         888         .8' `888.
#  888      888   .88ooo8888.        888        .88ooo8888.
#  888     d88'  .8'     `888.       888       .8'     `888.
# o888bood8P'   o88o     o8888o     o888o     o88o     o8888o

def testData(buffer, bufferSize):
    """Function achieving and testing data"""
    crcBuffer = b''
    values = []
    i = 0
    if (len(buffer)%2) != 0 :
        return
    while i<len(buffer):
        crcBuffer += buffer[i] + buffer[i+1]
        values.append(int.from_bytes(buffer[i] + buffer[i+1], byteorder='little'))
        i += 2
    if CrcModbus.calc(crcBuffer) == 0:
        
        triggerPoint = values[-2]
        values = values[:-3]
        buffer[:] = []

        global YS

        if triggerPoint <= bufferSize/2 :
            YS = values[triggerPoint +int(bufferSize/2) -1:] + values[:triggerPoint-1] + values[triggerPoint-1:triggerPoint+int(bufferSize/2)-1]
        else:
            YS = values[triggerPoint-int(bufferSize/2)-1:triggerPoint-1] + values[triggerPoint - 1:] + values[:triggerPoint-int(bufferSize/2)-1]
        YS[:] = [x / 204.8 for x in YS]
        global DATA_AVAILABLE
        DATA_AVAILABLE = True
    else:
        print(CrcModbus.calc(crcBuffer))
    return


def dataProcess():
    """Function processing data"""
    bufferSize = BUFFERSIZE
    buffer = []
    while RUN:
        value = COM.read(1)
        if value == b'\n':
            testData(buffer, bufferSize)
        else:
            buffer.append(value)
        if len(buffer) > (bufferSize+2)*2:
            del buffer[0]
    print(buffer)
    print(len(buffer))


BUFFERSIZE = 800
RUN = True

#   .oooooo.    ooooo     ooo ooooo
#  d8P'  `Y8b   `888'     `8' `888'
# 888            888       8   888
# 888            888       8   888
# 888     ooooo  888       8   888
# `88.    .88'   `88.    .8'   888
#  `Y8bood8P'      `YbodP'    o888o


def period_chosen(value):
    """Function creating period dropdown menu"""
    COM.write(b'P' + str.encode(str(value)) + b'\n')
    global PERIOD
    PERIOD = value
    return

def treshold_chosen(value):
    """Function creating treshold dropdown menu"""
    COM.write(b'T' + str.encode(str(int(value*204.8))) + b'\n')
    global TRESHOLD
    TRESHOLD = value
    return

def create_text_label():
    """Function creating text label"""
    label = Tk.Label(text=u"Choose port:")
    label.pack(side=Tk.TOP)
    return label


def create_port_menu():
    """Function creating drop down menu for port selection"""
    ports = list(port_list.comports())
    selected_port = Tk.StringVar()
    drop_down_menu = Tk.OptionMenu(
        APP_WINDOW, selected_port, *ports, command=port_chosen)
    drop_down_menu.pack(side=Tk.TOP)
    return drop_down_menu

def period_menu():
    """Function creating drop down menu for port selection"""
    periods = [35,50,100,250,500,1000,2000,4000,8000,16000,32000,64000,128000,256000,512000,1024000]
    selected_period = Tk.StringVar(APP_WINDOW)
    selected_period.set(periods[0])
    drop_down_menu = Tk.OptionMenu(
        APP_WINDOW, selected_period, *periods, command=period_chosen)
    drop_down_menu.pack(side = Tk.BOTTOM)
    return drop_down_menu

def treshold_menu():
    """Function creating drop down menu for port selection"""
    tresholds = [0.1,0.5,1.0,1.5,2.0,2.5,3.0,3.5,4.0,4.5,4.9]
    selected_treshold = Tk.StringVar(APP_WINDOW)
    selected_treshold.set(tresholds[5])
    drop_down_menu = Tk.OptionMenu(
        APP_WINDOW, selected_treshold, *tresholds, command=treshold_chosen)
    drop_down_menu.pack(side = Tk.LEFT)
    return drop_down_menu

def create_canvas():
    """Function creating CANVAS variable"""
    matplotlib.use('TkAgg')
    figure = Figure(figsize=(5, 4), dpi=100)
    subplot = figure.add_subplot(111)
    canvas = FigureCanvasTkAgg(figure, master=APP_WINDOW)
    canvas.get_tk_widget().pack(side=Tk.BOTTOM, anchor=Tk.W, fill=Tk.X, expand=Tk.YES)
    return canvas


def _quit():
    """Function called to quit app"""
    RUN = False
    try:
        COM.close()
    except:
        pass
    APP_WINDOW.quit()
    APP_WINDOW.destroy()
    THREAD1.join()


def on_key_event(event):
    """Function called on event of pressed key"""
    if event.keysym == "space":
        global PLAY
        PLAY = not PLAY
    elif event.keysym == "Escape":
        _quit()

PERIOD = 35
TRESHOLD = 2.5

APP_WINDOW = Tk.Tk()
APP_WINDOW.wm_title("PyScope")
APP_WINDOW.bind("<Key>", on_key_event)

CANVAS = None
TEXT_LABEL = create_text_label()
PORT_MENU = create_port_menu()

APP_WINDOW.mainloop()


