#include <TimerOne.h>
#include <FastCRC.h>
//#define DEBUG

FastCRC16 CRC16;

#define FASTADC 1

// defines macro "functions" for setting and clearing register bits
#ifndef cbi
#define cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#endif
#ifndef sbi
#define sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))
#endif
#ifndef tbi
#define tbi(sfr, bit) (_SFR_BYTE(sfr) ^= _BV(bit))
#endif


#define MAXIMUM_INDEX 800


// global variables
uint16_t index = 0;
uint16_t pointer = 0;
uint16_t buffer[MAXIMUM_INDEX + 1] = { 0, };
uint16_t oldVal = 0;
uint16_t messageCounter = 0;

uint16_t *triggerPoint = &(buffer[MAXIMUM_INDEX]);


uint8_t crc[2] = { 0 };

uint16_t counter = 0;
uint16_t treshold = 512;
bool triggered = false;

uint8_t input;

bool dataReady = false;
uint16_t failedRequests =0;

void setup()
{
    //initialization

#if FASTADC
    // set ADC prescaler to 16, speeding ADC reading to fastest accurate measuring
    sbi(ADCSRA, ADPS2);
    cbi(ADCSRA, ADPS1);
    cbi(ADCSRA, ADPS0);
#endif
    pinMode(13, OUTPUT);
    Timer1.initialize(1000);	// initialize timer1, value is in us
    Timer1.attachInterrupt(callback);	// attaches callback() as a timer overflow interrupt

    Serial.begin(115200);

    cbi(PORTB, PB5);
}

void loop()
{
      if (dataReady==true) {
#ifdef DEBUG
        Serial.println("Sending");
#else
        Serial.write((uint8_t *) messageCounter++, 2);
        Serial.write((uint8_t *) buffer, (MAXIMUM_INDEX + 1) * 2);
        ((uint16_t *) crc)[0] = CRC16.modbus((uint8_t *) buffer, (MAXIMUM_INDEX + 1) * 2);
        Serial.write(crc, 2);
        Serial.write('\n');
#endif
        counter = 0;
        index = 0;
        triggered = false;
        dataReady = false;   
    }

    while (Serial.available()) {
        input = Serial.read();
        switch(input){
            case ('P'):
                {
                    long input = Serial.parseInt();
                    bool settings_request = true;
                    if (input < 32)
                        input = 32;
                    Serial.print(input);
                    Timer1.setPeriod(input);  // set interrupt of timer1, value is in us, minimum is 32us
                    counter = 0;
                    index = 0;
                    triggered = false;
                    break;
                }
            case ('T'):
                {
                    uint16_t input = (uint16_t) Serial.parseInt();
                    bool settings_request = true;

                    //prejmenovat input, dodělat podmínku
                    if (input < 0 || input > 1023)
                        input = 512;
                    Serial.print(input);
                    treshold = input; // set interrupt of timer1, value is in us, minimum is 32us
                    counter = 0;
                    index = 0;
                    triggered = false;
                    break;
                }
            default:
                failedRequests++;
        }
    }
}

void callback()
    //Timer interrupt function for getting and sending data
{
    tbi(PORTB, PB5);
    uint16_t newVal = analogRead(0);
    //Read value
    if (!dataReady) {
        buffer[index++] = newVal;
        if (index >= MAXIMUM_INDEX)
            index = 0;

        if (triggered) {
            if (counter < MAXIMUM_INDEX / 2)
                counter++;
            else {
#ifdef DEBUG
                Serial.println("ReadyToSend");
#endif
                dataReady = true;
            }
            return;
        } else {
            //if not enough measured, just measure
            if (counter < MAXIMUM_INDEX / 2)
                counter++;
            //if enough measured, try to trigger
            else {
                if(index == 0){
                  oldVal = buffer[MAXIMUM_INDEX - 3];
                } else if(index == 1){
                  oldVal = buffer[MAXIMUM_INDEX - 2];
                } else {
                  oldVal = buffer[index - 2];
                }

              
                if (newVal >= treshold && oldVal < treshold) {
                    *triggerPoint = index - 1;
#ifdef DEBUG
                    Serial.print("Triggered ");
                    Serial.print(index);
                    Serial.print(" ");
                    Serial.print(buffer[index - 1]);
                    Serial.print(" ");
                    Serial.println(buffer[index - 2]);
#endif
                    triggered = true;
                    counter = 0;
                }
            }
        }
    }

}
